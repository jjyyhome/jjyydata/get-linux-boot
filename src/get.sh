#!/usr/bin/env bash
git submodule init

git submodule add https://github.com/kkoojjyy/fedora.git ../fedora

git submodule add https://github.com/kkoojjyy/netboot.git  ../bnetboot

git submodule add  https://github.com/kkoojjyy/netboot.xyz.git ../netboot.xyz

git submodule add  https://github.com/kkoojjyy/netboot.xyz-custom.git ../netboot-xyz-custom

git submodule add https://github.com/kkoojjyy/NetSUS.git ../netsus


git submodule add https://github.com/kkoojjyy/x11vnc-desktop.git ../x11vnc-desktop

git submodule add https://github.com/kkoojjyy/docker-novnc.git ../docker-novnc

git submodule sync

git submodule update
